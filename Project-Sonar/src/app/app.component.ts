import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'Project-Sonar';

  constructor(private readonly service: ServiceWorker) {

  }

  prueba =  {
    data:[
      {
        processData:'2525',
        scatocu:'Tecnicos',
      }
      ,
      {
        processData:'2525',
        scatocu:'Tecnicos34',
      },
      {
        processData:'2525',
        scatocu:'Tecnicos2',
      },
      {
        processData:'2525',
        scatocu:'Tecnicos12',
      }
    ]
  }

  getCategories = (): any => {
    this.service.test().pipe(
      map(data =>
        data.map(e => {
          try {
            const var23 = e.cat.split('#');
            return {
              ...e,
              code: var23[1],
              name: var23[0],
              id: var23.valid
            }
          } catch (error) { }
        }))
    )
  }
}
